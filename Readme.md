# libgbasio
small header only C library for GBA serial IO

## Usage
### Multiplayer mode
minimal example:
```c
#include "sio/multi.h"

sio_multi_init(Baudrate9600);
while(!sio_multi_ready());
bool parent = !sio_multi_is_child();
// child
sio_multi_send(value);
// parent
sio_multi_transfer(value);

unsigned short value = sio_multi_recv(2);
```
Use `sio_multi_init(baud)` to initialize with the specified baudrate.
Use `sio_multi_ready()` to wait for a stable connection (you probably wanna have some feedback to the player here).
Use `sio_multi_is_child()` to figure out who the parent is (the parent has to start all transfers).
Use `sio_multi_send(value)` to prepare the value for the next transfer.
Use `sio_multi_transfer(value)` to start a transfer with the value (parent only).
Use `sio_multi_recv(id)` to get the value transmitted by GBA id(0-3).
Use `sio_multi_id()` to get the GBA's id (only available after the first transfer finished).
Children will return -1 until the id is valid.
Use `sio_multi_busy()` to check transfer status.


