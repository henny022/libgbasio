#ifndef GBASIO_REGS_H
#define GBASIO_REGS_H

#define SIO_REG(type, offset) (*((volatile type*)(0x04000000 + (offset))))

#define SIO_REG_RCNT SIO_REG(unsigned short, 0x134)
#define SIO_RCNT_NORMAL 0x0000
#define SIO_RCNT_MULTI  0x0000
#define SIO_RCNT_UART   0x0000
#define SIO_RCNT_JOYBUS 0xc000
#define SIO_RCNT_GPIO   0x8000

#define SIO_REG_SIOCNT SIO_REG(unsigned short, 0x128)
#define SIO_SIOCNT_MULTI             0x2000
#define SIO_SIOCNT_IRQ_ENABLE        0x4000
#define SIO_SIOCNT_MULTI_BAUD_9600   0x0000
#define SIO_SIOCNT_MULTI_BAUD_38400  0x0001
#define SIO_SIOCNT_MULTI_BAUD_57600  0x0002
#define SIO_SIOCNT_MULTI_BAUD_115200 0x0003
#define SIO_SIOCNT_MULTI_CHILD       0x0004
#define SIO_SIOCNT_MULTI_READY       0x0008
#define SIO_SIOCNT_MULTI_ID          0x0030
#define SIO_SIOCNT_MULTI_ERROR       0x0040
#define SIO_SIOCNT_MULTI_START       0x0080
#define SIO_SIOCNT_MULTI_BUSY        0x0080

#define SIO_REG_MULTI_SEND  SIO_REG(unsigned short, 0x12a)
#define SIO_REG_MULTI_RECV  ((volatile unsigned short*)(0x04000000 + (0x120)))
#define SIO_REG_MULTI_RECV0 SIO_REG(u16, 0x120)
#define SIO_REG_MULTI_RECV1 SIO_REG(u16, 0x122)
#define SIO_REG_MULTI_RECV2 SIO_REG(u16, 0x124)
#define SIO_REG_MULTI_RECV3 SIO_REG(u16, 0x126)

#endif //GBASIO_REGS_H
