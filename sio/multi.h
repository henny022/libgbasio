#ifndef GBASIO_MULTI_H
#define GBASIO_MULTI_H

#include "regs.h"

enum Baudrate
{
    Baudrate9600 = SIO_SIOCNT_MULTI_BAUD_9600,
    Baudrate38400 = SIO_SIOCNT_MULTI_BAUD_38400,
    Baudrate57600 = SIO_SIOCNT_MULTI_BAUD_57600,
    Baudrate115200 = SIO_SIOCNT_MULTI_BAUD_115200,
};

static inline void sio_multi_init(enum Baudrate baudrate)
{
    SIO_REG_RCNT = SIO_RCNT_MULTI;
    SIO_REG_SIOCNT = SIO_SIOCNT_MULTI | baudrate;
}

static inline int sio_multi_ready()
{
    return SIO_REG_SIOCNT & SIO_SIOCNT_MULTI_READY;
}

static inline int sio_multi_is_child()
{
    return SIO_REG_SIOCNT & SIO_SIOCNT_MULTI_CHILD;
}

static inline void sio_multi_send(unsigned short value)
{
    SIO_REG_MULTI_SEND = value;
}

static inline void sio_multi_transfer(unsigned short value)
{
    SIO_REG_MULTI_SEND = value;
    SIO_REG_SIOCNT |= SIO_SIOCNT_MULTI_START;
}

static inline unsigned short sio_multi_recv(int id)
{
    return SIO_REG_MULTI_RECV[id];
}

static inline int sio_multi_busy()
{
    return SIO_REG_SIOCNT & SIO_SIOCNT_MULTI_BUSY;
}

static inline int sio_multi_id()
{
    int id = SIO_REG_SIOCNT & SIO_SIOCNT_MULTI_ID;
    id = id >> 4;
    if (sio_multi_is_child() && id == 0)
    {
        return -1;
    }
    return id;
}

#endif //GBASIO_MULTI_H
